// Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Jawaban 2

var panjangBuku = books.length;
var time = 10000;

function getSisaWaktu(time, i, panjangBuku) {
    readBooksPromise(time, books[i])
        .then(function (remainingTime) {
            time = remainingTime;
            panjangBuku = panjangBuku - 1;
            if (panjangBuku > 0) {
                getSisaWaktu(time, i+1, panjangBuku);
            }
        })
        .catch(function (error) {
            
        })
}

getSisaWaktu(time, 0, panjangBuku);