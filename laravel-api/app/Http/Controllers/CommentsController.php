<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;
use App\Events\CommentStoreEvent;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comment 
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::create([
            'content'  => $request->content,
            'post_id'  => $request->post_id
        ]);

        //memanggil event CommentStoreEvent
        event(new CommentStoreEvent($comment));

        // //dikirim ke yang memiliki post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        // //dikirim yang mengirim komentar
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comments::find($id);

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Comment',
                'data'    => $comment 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Comment ' . $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::find($id);

        if($comment) {
            $user = auth()->user();
            if ($comment->user_id != $user->id) {
                return response()->json([
                'success' => false,
                'message' => 'Post  bukan milik anda', 
            ], 403);
            }
            $comment->update([
                'content'  => $request->content,
                'post_id'  => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment  ' . $comment->name .'  Updated',
                'data'    => $comment  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comments::find($id);

        if($comment) {
            $user = auth()->user();
            if ($comment->user_id != $user->id) {
                return response()->json([
                'success' => false,
                'message' => 'Post  bukan milik anda', 
            ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
