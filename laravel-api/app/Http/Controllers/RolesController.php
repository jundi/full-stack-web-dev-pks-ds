<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data'    => $roles  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = roles::create([
            'name'  => $request->name,
        ]);

        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Roles Created',
                'data'    => $roles
            ], 201);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Roles Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = roles::find($id);

        if($roles){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Roles',
                'data'    => $roles 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Roles ' . $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $roles = roles::find($id);

        if($roles) {
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Roles  ' . $roles->name .'  Updated',
                'data'    => $roles  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Roles Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = roles::find($id);

        if($roles) {
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Roles Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Roles Not Found',
        ], 404);
    }
}
