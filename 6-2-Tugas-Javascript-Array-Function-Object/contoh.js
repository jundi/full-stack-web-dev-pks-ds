// Array
var sayHello = "Hello World!" 
console.log(sayHello)

var hobbies = ["coding", "cycling", "climbing", "skateboarding"] 
console.log(hobbies)
console.log(hobbies.length)
console.log(hobbies[0])
console.log(hobbies[2])
console.log(hobbies[hobbies.length -1])

var numbers = [0, 1, 2]
numbers.push(3)
console.log(numbers)

var numbers = [0, 1, 2, 3, 4, 5]
numbers.pop() 
console.log(numbers)

var numbers = [0, 1, 2, 3]
numbers.unshift(-1) 
console.log(numbers)

var numbers = [ 0, 1, 2, 3]
numbers.shift()
console.log(numbers)

var animals = ["kera", "gajah", "musang"] 
animals.sort()
console.log(animals)

var angka = [0, 1, 2, 3, 4, 5, 6, 7]
var irisan1 = angka.slice(2,6) 
console.log(irisan1)

var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()
console.log(salinAngka)

var fruits = [ "banana", "orange", "grape"]
fruits.splice(1, 0, "watermelon") 
console.log(fruits)

var fruits = [ "banana", "orange", "grape"]
fruits.splice(0, 2)
console.log(fruits)

var biodata = "name:john,doe" 
var name = biodata.split(":")
console.log(name)

var biodata = " fatkhurrahman jundi binauf " 
var name = biodata.trim().split(" ")
console.log(name)

var title = ["my", "first", "experience", "as", "programmer"] 
var slug = title.join("-")
console.log(slug)

// Function
function tampilkan() {
  console.log("halo!");
}
 
tampilkan();

function myfunction() {
  var a = 3
  var b = 2
  return a + b
}
 
console.log(myfunction());

function jumlah(a, b) {
  return a + b
}
 
console.log(jumlah(5, 9));

function operasi(a, b, jenis = "penjumlahan") {
    var c
  if(jenis == "penjumlahan"){
    c = a + b
  }else if(jenis == "pengurangan"){
    c = a - b
  }
  return c
}
 
console.log(operasi(5, 9, "pengurangan"));

// Object
var car2 = {}
car2.brand = "Lamborghini"
car2.type = "Sports Car"
car2.price = 100000000 
car2["hp"] = 730

console.log(car2)

// Array Iteration
var mobil = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}
]

mobil.forEach(function(item){
    console.log(item)
})

var newCars = mobil.map(function(merek){
    return merek.merk
})
console.log(newCars)

var newType = mobil.map(function(item){
    return {merk:item.merk,warna:item.warna,tipe:"sedan"}
})
console.log(newType)

var newType = mobil.filter(function(item){
    return item.tipe != "sedan"
})
console.log(newType)